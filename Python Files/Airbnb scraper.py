from bs4 import BeautifulSoup
import time
import pandas as pd
import requests

def get_airbnb_listings (listings_per_page, total_listings, output_filepath):

    ''' 
    This function retrieves key data points for airbnb properties in the Thanet district in South East Kent 
    Please Enter the number of airbnb properties per page and the total number of properties in the results as arguments
    '''

    list_of_dictionaries = []
    title_list = []
    listing_count = 0

    try:
        #looping through pages - 18 listings per page which is the offset for the url
        for offset in range(0,total_listings,listings_per_page):

            link_list = []
            url = r'https://www.airbnb.co.uk/s/Thanet-District/homes?items_offset=' + str(offset) + r'&pagination_search=true'
            response = requests.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            listings = soup.find_all('div', class_="c4mnd7m dir dir-ltr")
            
            #looping through each listing to get the link for the listing url 
            for listing in listings:
                link = listing.find('a', href = True)
                full_link = r'https://www.airbnb.co.uk/' + link['href']
                link_list.append(full_link)
            
            #looping through listings to gain price, host, rating and room type data
                try:
                    total_price = listing.find('div', class_="_i5duul").text
                    total_price = total_price.split('·')
                    night_price = total_price[0]
                except:
                    night_price = 'n/a'

                try:
                    rating = listing.find('span', class_="r1dxllyb dir dir-ltr").text
                except:
                    rating = 'n/a'

                try:
                    host = listing.find('div', class_ ="f15liw5s s1cjsi4j dir dir-ltr").text
                except:
                    host= 'n/a'

                try:
                    room_type = listing.find('div', class_="t1jojoys dir dir-ltr").text
                except:
                    room_type = 'n/a'
                
                # creating a dictionary for data points and creating a list of dictionaries per listing
                listing_dict = {'Type': room_type,
                                'Price': night_price,
                                'Rating': rating,
                                'Host': host,
                                'Link': full_link
                                }
                list_of_dictionaries.append(listing_dict)

                listing_count += 1
                print(listing_count)

            #looping through the link list to extract the title info from the listing page
            for full_link in link_list:
                r_link = requests.get(full_link)
                soup_link = BeautifulSoup(r_link.content, 'html.parser').text
                soup_link = soup_link.strip("\n")
                soup_link = soup_link.split (',')
                soup_link = soup_link[0:-2]
                title = ' | '.join(soup_link)
                title_list.append(title)
                
    except:
        pass

    #creating dataframe, adding title and saving to csv
    listing_df = pd.DataFrame(list_of_dictionaries)
    title_series = pd.Series(title_list)

    try:
        listing_df['Title'] = title_series.values
        listing_df.to_csv(output_filepath, index = False, encoding = 'utf-8')
        return listing_df

    except:
        listing_df.to_csv(output_filepath, index = False, encoding = 'utf-8')
        print('INDEX ERROR')
        return listing_df, title_series

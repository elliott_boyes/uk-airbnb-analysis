import pandas as pd
from Get_Kent_Towns import get_kent_towns

def clean_sales_data (input_filepath, output_filepath):

    joined_data = pd.read_csv(input_filepath)
    town_list = get_kent_towns()

    #Remove uncessary text from columns
    joined_data['Type'] = joined_data['Type'].apply(lambda x: x[:-8] if x[-8:] == 'for sale' else x)
    joined_data['Price'] = joined_data['Price'].apply(lambda x: x[1:])
    

    #extract town from Address columm
    joined_data['City'] = 'Kent'
    for i, t in enumerate(joined_data['Address']):
        for p in town_list:
            if p in t:
                joined_data['City'][i] = p 

    #extract number of bedrooms and property type
    type_list = joined_data['Type']
    bedroom_list = []
    property_type_list = []

    for t in type_list:
        items = t.split()

        if len(items) > 1:
            if 'bedroom' in items[1]:
                bedroom_list.append(items[0])
                property_type_list.append(t[10:])
            else:   
                bedroom_list.append('n/a')
                property_type_list.append(t)
        else:   
            bedroom_list.append('n/a')
            property_type_list.append(t)
        

    #Add town, county and country columns for Tableau analysis 
    joined_data['Bedrooms'] = bedroom_list
    joined_data['Property Type'] = property_type_list
    joined_data['County'] = 'Kent'
    joined_data['Country'] = 'United Kingdom'
    clean_sales_df = joined_data.rename(columns={'Address': 'Full Address'})
    clean_sales_df = clean_sales_df.fillna('n/a')
    clean_sales_df = clean_sales_df.drop(columns = ['Unnamed: 0', 'Type'])
    clean_sales_df.to_csv(output_filepath, encoding = 'utf-8')
    return clean_sales_df

import pandas as pd 
import numpy as np
from Get_Kent_Towns import get_kent_towns

def clean_airbnb_data (input_filepath, output_filepath):
    #Loading and cleaning airbnb data
    town_list = get_kent_towns()
    airbnb_data = pd.read_csv(input_filepath)

    #splitting Type column into type and location 
    airbnb_data['Type'] = airbnb_data['Type'].str.replace(' in', ',')
    airbnb_data['Location'] = [l.split(',')[1].strip() for l in airbnb_data['Type'] ]
    airbnb_data['Type']  = [t.split(',')[0].strip() for t in airbnb_data['Type'] ]

    #clean price column
    airbnb_data['Price'] = airbnb_data['Price'].str.replace('night', ',')
    airbnb_data['Price'] = airbnb_data['Price'].str.replace('per', '')
    airbnb_data['Price'] = [p.split(',')[1].strip() if type(p) == str else p for p in airbnb_data['Price']]
    airbnb_data['Price'] = airbnb_data['Price'].apply(lambda x: str(x)[1:])
    airbnb_data['Price'] = airbnb_data['Price'].str.replace('an', 'n/a')

    #Clean Rating and Reviews
    airbnb_data['Rating'] = [r.split()[0].strip() if type(r) == str  else r  for r in airbnb_data['Rating']]
    airbnb_data['Reviews'] = [str(r).split()[1].strip() if len(str(r).split()) > 1  else r  for r in airbnb_data['Rating']]

    #extract town from title columm
    for i, t in enumerate(airbnb_data['Title']):
        for p in town_list:
            if p in t:
                airbnb_data['Title'][i] = p 
        if airbnb_data['Title'][i] not in town_list:
            airbnb_data['Title'][i] = 'Kent'

    #combine title and location into one column
    airbnb_data['City'] = np.select([(airbnb_data['Title'] == 'Kent'), (airbnb_data['Title'] != 'Kent')] , [airbnb_data['Location'], airbnb_data['Title'] ] )
    airbnb_data['City'] = airbnb_data['City'].str.replace('Dane Valley', 'Margate')
    airbnb_data['City'] = airbnb_data['City'].str.replace('Westbrook', 'Westgate-on-sea')
    airbnb_data['County'] = 'Kent'
    airbnb_data['Country'] = 'United Kingdom'
    airbnb_data = airbnb_data.fillna('n/a')
    airbnb_data = airbnb_data.drop(columns = ['Title', 'Location'])
    airbnb_data.to_csv(output_filepath)
    return airbnb_data


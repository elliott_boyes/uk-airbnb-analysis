from bs4 import BeautifulSoup
import time
import pandas as pd
import requests

def get_for_sale_properties (listings_per_page, total_listings):
    property_list = []
    property_count = 0

    try:
        for index in range(0,total_listings, listings_per_page):

            # No filter
            # url = r'https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D&index=' + str(index) + r'&propertyTypes=bungalow%2Cdetached%2Cflat%2Csemi-detached%2Cterraced&mustHave=&dontShow=&furnishTypes=&keywords='
            
            # price high --> low filter
            # url = r'https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D&index=' + str(index) + r'&propertyTypes=bungalow%2Cdetached%2Cflat%2Csemi-detached%2Cterraced&mustHave=&dontShow=&furnishTypes=&keywords='

            #price low --> high filter
            # url = r'https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D&sortType=1&index=' + str(index) +  r'&propertyTypes=bungalow%2Cdetached%2Cflat%2Csemi-detached%2Cterraced&mustHave=&dontShow=&furnishTypes=&keywords='
            
            # newest --> oldest
            # url = r'https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D&sortType=6&index=' + str(index) + r'&propertyTypes=bungalow%2Cdetached%2Cflat%2Csemi-detached%2Cterraced&mustHave=&dontShow=&furnishTypes=&keywords='

            # oldest --> newest
            url = r'https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D&sortType=10&index=' + str(index) + r'&propertyTypes=bungalow%2Cdetached%2Cflat%2Csemi-detached%2Cterraced&mustHave=&dontShow=&furnishTypes=&keywords='
            
            response = requests.get(url)
            soup = BeautifulSoup(response.content, 'html.parser')
            listings = soup.find_all('div', class_="l-searchResult is-list")
            

            for listing in listings:

                property_info = listing.find('div', class_ ="propertyCard-details").text
                property_info = property_info.split("\n")
                property_type = property_info[3].strip()
                address = property_info[7].strip()
                

                price = listing.find('div', class_="propertyCard-priceValue").text
                price = price.strip("\n")
                    
                property_dict = { 'Type': property_type,
                                    'Address': address,
                                    'Price': price,
                                }
                property_list.append(property_dict)

                property_count +=1
                print(property_count)
    except:
        pass

        time.sleep(1)

    property_df = pd.DataFrame(property_list)
    property_df.to_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Rightmove_sale_raw_oldest.csv', index = False, encoding = 'utf-8')
    return property_df

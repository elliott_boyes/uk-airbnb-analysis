from bs4 import BeautifulSoup
import time
import pandas as pd
import requests

def get_rental_properties (listings_per_page, total_listings, ouput_filepath):
    property_list = []
    property_count = 0

    for index in range(0,total_listings, listings_per_page):

        url = r'https://www.rightmove.co.uk/property-to-rent/find.html?keywords=&sortType=6&viewType=LIST&channel=RENT&index=' + str(index) + r'&radius=0.0&locationIdentifier=USERDEFINEDAREA%5E%7B%22id%22%3A8187913%7D'
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        listings = soup.find_all('div', class_="l-searchResult is-list")
        

        for listing in listings:

            property_info = listing.find('div', class_ ="propertyCard-details").text
            property_info = property_info.split("\n")
            property_type = property_info[3].strip()
            address = property_info[7].strip()

            monthly_price = listing.find('div', class_="propertyCard-rentalPrice-primary").text
            monthly_price = monthly_price.strip("\n")
            
            weekly_price = listing.find('div', class_ ="propertyCard-rentalPrice-secondary").text
            weekly_price = weekly_price.strip("\n")
            

            property_dict = { 'Type': property_type,
                            'Address': address,
                            'Monthly Price': monthly_price,
                            'Weekly Price': weekly_price
                            }
            property_list.append(property_dict)

            property_count +=1
            print(property_count)

    property_df = pd.DataFrame(property_list)
    property_df.to_csv(ouput_filepath, index = False, encoding = 'utf-8')
    return property_df.head()



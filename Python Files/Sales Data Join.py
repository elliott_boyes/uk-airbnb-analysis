import pandas as pd

def union_sales_data (output_filepath):

    # concatenate all results together then remove duplicate rows which show up in 2 or more searches
    data = pd.read_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Raw Data\Rightmove_for_sale_raw_data.csv')
    high_data = pd.read_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Raw Data\Rightmove_sale_raw_high_price.csv')
    low_data = pd.read_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Raw Data\Rightmove_sale_raw_low_price.csv')
    new_data = pd.read_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Raw Data\Rightmove_sale_raw_newest.csv')
    old_data = pd.read_csv(r'C:\Users\ellio\OneDrive\Desktop\Kubrick Bench Work\Raw Data\Rightmove_sale_raw_oldest.csv')
    
    joined_data = pd.concat([data, high_data, low_data, new_data, old_data], axis = 0, ignore_index= True)
    joined_data= joined_data.drop_duplicates()
    joined_data.to_csv(output_filepath)
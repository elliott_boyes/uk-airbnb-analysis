import requests
from bs4 import BeautifulSoup

#retrieving list of Kentish towns from wikipedia
def get_kent_towns():

    url = r'https://simple.wikipedia.org/wiki/List_of_places_in_Kent'
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')

    table = soup.find('div', class_="mw-parser-output")
    places = table.find_all(href = True)

    town_list = [p.text for p in places]
    town_list = town_list[36:]
    return town_list
import pandas as pd
import numpy as np
from Get_Kent_Towns import get_kent_towns

def clean_rental_data (input_filepath, output_filepath):
    rental_data = pd.read_csv(input_filepath)
    town_list = get_kent_towns()

    rental_data['Type'] = rental_data['Type'].apply(lambda x: x[:-8] if x[-8:] == 'for sale' else x)
    rental_data['Monthly Price'] = rental_data['Monthly Price'].apply(lambda x: x[1:-4])
    rental_data['Weekly Price'] = rental_data['Weekly Price'].apply(lambda x: x[1:-3])
    rental_data = rental_data.applymap(lambda x: x.strip())

    #extract town from Address columm
    rental_data['City'] = 'Kent'
    for i, t in enumerate(rental_data['Address']):
        for p in town_list:
            if p in t:
                rental_data['City'][i] = p 
            
    #extract number of bedrooms and property type from type using loop through list
    type_list = rental_data['Type']
    bedroom_list = []
    property_type_list = []

    for t in type_list:
        items = t.split()
        if len(items) > 1:
            if 'bedroom' in items[1]:
                bedroom_list.append(items[0])
                property_type_list.append(t[10:])
            else:   
                bedroom_list.append('n/a')
                property_type_list.append(t)
        else:   
            bedroom_list.append('n/a')
            property_type_list.append(t)
        
    #Add town, county and country columns for Tableau analysis 
    rental_data['Bedrooms'] = bedroom_list
    rental_data['Property Type'] = property_type_list
    rental_data['County'] = 'Kent'
    rental_data['Country'] = 'United Kingdom'
    clean_rental_df = rental_data.rename(columns={'Address': 'Full Address'})
    clean_rental_df = clean_rental_df.fillna('n/a')
    clean_rental_df = clean_rental_df.drop(columns = ['Type'])
    clean_rental_df.to_csv(output_filepath, encoding = 'utf-8')
    return clean_rental_df